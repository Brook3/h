local term_keys = '<leader>t'
return {
    {
        'akinsho/toggleterm.nvim',
        version = "*",
        event = "VeryLazy",
        keys = { term_keys },
        config = function()
            require("toggleterm").setup{
                open_mapping = [[<leader>t]],
                direction = 'float',
            }
        end
    },
}
