local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    require("plugins.term"),
    require("plugins.fzf"),
    require("plugins.treesitter"),
    require("plugins.edit"),
    require("plugins.find"),
    require("plugins.bufferline"),
    require("plugins.statusline"),
    require("plugins.outline"),
    require("plugins.notify"),
})
