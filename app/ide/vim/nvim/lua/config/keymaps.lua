vim.g.mapleader = " "

local mode_nv = { "n", "v" }
local mode_ci = { "c", "i" }
local mode_v = { "v" }
local mode_i = { "i" }
local mode_c = { "c" }
local nmappings = {
	-- copy
	{ from = "Y",             to = "\"+y",                                                                mode = mode_v },

	-- move
	{ from = "<C-a>", to = "<Home>", mode = mode_ci },
	{ from = "<C-e>", to = "<End>", mode = mode_ci },
	{ from = "<C-h>", to = "<left>", mode = mode_i },
	{ from = "<C-j>", to = "<down>", mode = mode_i },
	{ from = "<C-k>", to = "<up>", mode = mode_i },
	{ from = "<C-l>", to = "<right>", mode = mode_i },

	-- window
	{ from = "<leader>wh",     to = "<C-w>h", },
	{ from = "<leader>wj",     to = "<C-w>j", },
	{ from = "<leader>wk",     to = "<C-w>k", },
	{ from = "<leader>wl",     to = "<C-w>l", },

	-- buffer
	{ from = "<leader>bn",     to = ":bn<CR>", },
	{ from = "<leader>bp",     to = ":bp<CR>", },
	{ from = "<leader>bd",     to = ":bd<CR>", },
	{ from = "<leader>bl",     to = ":ls<CR>", },

	-- config
	{ from = "<leader>r",    to = ":source ~/.config/nvim/init.lua<CR>" },
}

for _, mapping in ipairs(nmappings) do
	vim.keymap.set(mapping.mode or "n", mapping.from, mapping.to, { noremap = true })
end

