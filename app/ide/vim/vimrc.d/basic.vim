"""""""""""""""""""""""""""""""""""""""""
" 基础配置设置
"""""""""""""""""""""""""""""""""""""""""
set shortmess+=c
set nolist                       " 不Show non-printable characters.
set nobackup                     "不自动保存
set nowritebackup
" set backupcopy=yes               " 修改不改动iNode 这个设置开启后有点傻逼了，保存之类的都得强制！
set noswapfile                   "不生成swap文件
" set paste                      "粘贴保留格式 coc要求不能设置
set relativenumber number        "相对行号，可用Ctrl+n在相对/绝对行号间切换
set history=2000                 "history存储长度
set nocompatible                 "非兼容vi模式,避免以前版本的一些bug和局限
set autoread                     "文件修改之后自动载入
" set t_ti= t_te=                  "退出vim后，内容显示在终端屏幕
set title                        "change the terminal's title
set vb t_vb=                     "当vim进行编辑时，如果命令错误，会发出警报，该设置去掉警报
set tm=500
set mat=2                        "Blink times every second when matching brackets
set showmatch                    "括号配对情况
set hidden                       "A buffer becomes hidden when it is abandoned
set wildmenu                     " 命令模式tab补全
set wildmode=longest:list,full   " 命令模式tab补全
set ttyfast                " Faster redrawing.
set lazyredraw             " Only redraw when necessary.
set wildignore=*.swp,*.,*.pyc,*.class
set scrolloff=5                  "至少有5行在光标所在行上下
set mouse-=a                     "不启用鼠标
set selection=old
set selectmode=mouse,key
set viminfo^=%                   "Remember info about open buffers on close
set magic                        "正则表达式匹配形式
set backspace=eol,start,indent   "Configure backspace so it acts as it should act
set whichwrap+=<,>,h,l
set splitbelow             " Open new windows below the current window.
set splitright             " Open new windows right of the current window.
set cursorline             " Find the current line quickly.
set cursorcolumn           " Find the current column quickly.
set wrapscan               " Searches wrap around end-of-file.
set report      =0         " Always report changed lines.
set synmaxcol   =200       " Only highlight the first 200 columns.

" clipboard
"""""""""""""""""""""""""""""""""""""""""
" set clipboard+=unnamed           "共享剪贴板 vim
" set clipboard+=unnamedplus       "共享剪贴板 nvim需要有xsel支持
" Copy to system clipboard
vnoremap Y "+y

" 搜索
"""""""""""""""""""""""""""""""""""""""""
set hlsearch                     "高亮search命中的文本。
set ignorecase                   "搜索时忽略大小写
set incsearch                    "随着键入即时搜索
set smartcase                    "有一个或以上大写字母时仍大小写敏感

" 缩进
"""""""""""""""""""""""""""""""""""""""""
filetype indent on               " 针对不同的文件类型采用不同的缩进格式
set smartindent                  " 智能缩进
set autoindent                   " 总是自动缩进
set tabstop=4                    " 设置Tab键的宽度(等同的空格个数)
set shiftwidth=4                 " 自动对齐的空格数
set softtabstop=4                " 按退格键时可以一次删掉4个空格
set smarttab                     " insert tabs on the start of a line according to shiftwidth, not tabstop
set expandtab                    " 将Tab自动转化成空格(需要输入真正的Tab键时,使用Ctrl+V+Tab)
set shiftround                   " Use multiple of shiftwidth when indenting with '<' and '>'

" 折叠
"""""""""""""""""""""""""""""""""""""""""
set foldlevel=99
" 基于缩进或语法进行代码折叠
"set foldmethod=indent
set foldmethod=syntax
" 启动 vim 时关闭折叠代码
set nofoldenable
" 操作：za，打开或关闭当前折叠；zM，关闭所有折叠；zR，打开所有折叠。

"编码
"""""""""""""""""""""""""""""""""""""""""
set encoding=utf-8
set fileencodings=ucs-bom,utf-8,cp936,gb18030,chinese,big5,euc-jp,euc-kr,latin1
set fileencoding=utf-8
set helplang=cn
set termencoding=utf-8           "这句只影响普通模式 (非图形界面) 下的Vim
set ffs=unix,dos,mac             "Use Unix as the standard file type
set formatoptions+=m             "如遇Unicode值大于255的文本，不必等到空格再折行。
set formatoptions+=B             "合并两行中文时，不在中间加空格：

" 撤销
"""""""""""""""""""""""""""""""""""""""""
" set undolevels=1000              "How many undos
" set undoreload=10000             "number of lines to save for undo
" if v:version >= 730
"     set undofile                 "keep a persistent backup file
"     set undodir=~/.vim/vimundo/
" endif

" 跳转 jumps
"""""""""""""""""""""""""""""""""""""""""
" set jumpoptions=stack

" tabline
"""""""""""""""""""""""""""""""""""""""""
set tabpagemax=16
set showtabline=0

" style
"""""""""""""""""""""""""""""""""""""""""
set background=dark    " Setting dark/light mode
