Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'

let g:db_ui_save_location = '/h/code/hcode/sql'

let g:db_ui_icons = {
    \ 'expanded': '▾',
    \ 'collapsed': '▸',
    \ 'saved_query': '*',
    \ 'new_query': '+',
    \ 'tables': '~',
    \ 'buffers': '»',
    \ 'connection_ok': '✓',
    \ 'connection_error': '✕',
    \ }
