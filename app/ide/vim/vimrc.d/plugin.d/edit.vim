"""""""""""""""""""""""""""""""""""""""""
""" quick edit
"""""""""""""""""""""""""""""""""""""""""
" 快速注释
"""""""""""""""""""""""""""""""""""""""""
Plug 'preservim/nerdcommenter'
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1
"基本操作：
"<space>ci选区内切换注释状态
"<space>cm选区内整体行注释
"<space>cn选区内注释
"<space>cs注释块
"<space>cu取消注释

" 括号配对
"""""""""""""""""""""""""""""""""""""""""
Plug 'tpope/vim-surround'
" 常用操作
"cs替换 ds删除 ys增加 vS|VS可视化增加

" 括号补全
"""""""""""""""""""""""""""""""""""""""""
Plug 'jiangmiao/auto-pairs', { 'for': ['vim-plug', 'html', 'js', 'css', 'php', 'go', 'python', 'c'] }

" 多点修改
Plug 'terryma/vim-multiple-cursors'
" 常用操作
" next <C-n>
" skip <C-x>
" prev <C-p>

" 快速移动
"""""""""""""""""""""""""""""""""""""""""
Plug 'easymotion/vim-easymotion'
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap <Leader>jw <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
nmap <Leader>jw <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" Undo Tree
Plug 'mbbill/undotree'

" mark
"""""""""""""""""""""""""""""""""""""""""
Plug 'kshenoy/vim-signature'	"基本操作：m,./[`/]`/m-/m<space>
