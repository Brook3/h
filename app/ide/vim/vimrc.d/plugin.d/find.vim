"""""""""""""""""""""""""""""""""""""""""
""" find
"""""""""""""""""""""""""""""""""""""""""
" 文件切换
" 1. 按目录树进行切换
"""""""""""""""""""""""""""""""""""""""""
" ranger
let vi_type = GetViType()
if vi_type == 'vim'
    Plug 'francoiscabrol/ranger.vim'
    let g:NERDTreeHijackNetrw = 0 " add this line if you use NERDTree
    let g:ranger_replace_netrw = 1 " open ranger when vim open a directory
    let g:ranger_command_override = 'ranger --cmd "set show_hidden=true"'
    map <leader>fm :Ranger<CR>
elseif vi_type == 'nvim'
    Plug 'kevinhwang91/rnvimr'
    " Map Rnvimr action
    let g:rnvimr_action = {
                \ 'gw': 'cd /h'
                \ }

    nnoremap <leader>fm :RnvimrToggle<CR>
endif



" 2. 按方法列表进行切换
"""""""""""""""""""""""""""""""""""""""""
Plug 'majutsushi/tagbar'
nmap <Leader>ft :TagbarToggle<cr>
"设置tagbar的窗口宽度
let g:tagbar_width=30
"开启自动预览(随着光标在标签上的移动，顶部会出现一个实时的预览窗口)
let g:tagbar_autopreview = 1
"关闭排序,即按标签本身在文件中的位置排序 s可切换
let g:tagbar_sort = 0
" tagbar 子窗口中不显示冗余帮助信息
let g:tagbar_compact=1
" Taglist
Plug 'liuchengxu/vista.vim'

" 3. 按文件名进行切换
"""""""""""""""""""""""""""""""""""""""""
" fzf
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
nmap <Leader>ff :FZF<cr>
nmap <Leader>fc :Rg<cr>
nmap <Leader>fb :Buffers<cr>
nmap <Leader>fh :History<cr>
" 设置快捷键打开fzf文件搜索
nnoremap <leader>f :Files<CR>
" 设置快捷键在fzf中切换搜索的目录
nnoremap <leader>cd :FZF_CWD<CR>

" 4. 按内容进行切换
"""""""""""""""""""""""""""""""""""""""""
" Rg
" ctrlsf
" Plug 'dyng/ctrlsf.vim'
" let g:ctrlsf_ackprg = 'rg'
" nmap <Leader>fs :CtrlSF<space>
