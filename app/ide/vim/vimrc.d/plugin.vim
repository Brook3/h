"""""""""""""""""""""""""""""""""""""""""
" 插件管理配置
"""""""""""""""""""""""""""""""""""""""""
" ===
" === Auto load for first time uses
" ===
call plug#begin('~/.local/share/vim/plugged')

" style
source ~/.config/vim/vimrc.d/plugin.d/style.vim

" edit
source ~/.config/vim/vimrc.d/plugin.d/edit.vim

" find
source ~/.config/vim/vimrc.d/plugin.d/find.vim

" 按键提示
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] }
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

" lsp
source ~/.config/vim/vimrc.d/plugin.d/lsp.vim

" dap
source ~/.config/vim/vimrc.d/plugin.d/dap.vim

"game
source ~/.config/vim/vimrc.d/plugin.d/game.vim

"""""""""""""""""""""""""""""""""""""""""
" 工程管理
"""""""""""""""""""""""""""""""""""""""""

" 环境恢复
"""""""""""""""""""""""""""""""""""""""""
" 保存快捷键
" map <leader>sa :mksession! ~/.vim/my.session<cr> :wviminfo! ~/.vim/my.viminfo<cr>
" 恢复快捷键
" map <leader>ra :source ~/.vim/my.session<cr> :rviminfo ~/.vim/my.viminfo<cr>

call plug#end()            " required

""" 插件加载后的配置
"""""""""""""""""""""""""""""""""""""""""
" plug
nnoremap <leader>pi :PlugInstall<cr>
nnoremap <leader>ps :PlugStatus<cr>
nnoremap <leader>pu :PlugUpdate<cr>
nnoremap <leader>pc :PlugClean<cr>

" 检查 gruvbox 是否可用  
if exists(":colorscheme") && exists(":GruvboxLoad")  
    colorscheme gruvbox
    GruvboxLoad!
endif

" 背景透明
highlight Normal guibg=NONE ctermbg=None
