# create sa account
kubectl -n devops create serviceaccount monitor

# bind
kubectl create clusterrolebinding monitor-clusterrolebinding monitor --clusterrole=cluster-admin --serviceaccount=devops:monitor
