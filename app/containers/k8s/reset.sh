#!/bin/bash

sudo kubeadm reset

sudo kubeadm init --config ${H}/app/containers/k8s/kubeadm/kubeadm-config.yaml

# mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config

# 解除主机不能跑pod的限制
kubectl taint nodes --all node-role.kubernetes.io/master-
